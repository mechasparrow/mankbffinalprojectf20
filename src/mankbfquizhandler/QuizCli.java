/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author mechasparrow
 */
public class QuizCli {

    private static final boolean CHEAT = true;
    private static final boolean ONLINE = false;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        QuestionLoader questionLoader = null;
        
        if (ONLINE){
            System.out.println("ONLINE MODE");
            questionLoader = new OnlineQuestionLoader();
        }else{
            System.out.println("OFFLINE MODE");
            questionLoader = new OfflineQuestionLoader(18);
        }
        
        ArrayList<BasicQuestion> triviaQuestions = questionLoader.loadQuestions();
        if (triviaQuestions == null){
            System.out.println("Error");
            return;
        }
        //NOTE must shuffle answers
        
        for (BasicQuestion question : triviaQuestions){
            displayQuestion(question);
            
            if (CHEAT){
                System.out.println(question.getCorrectAnswer());
            }
            
            Integer answerResponse = promptForAnswer(question);
            
            if (answerResponse <= 0){
                break;
            }
            
            Boolean correct = checkAnswer(question, answerResponse);
            
            if (correct){
                System.out.println("correct");
            }else{
                System.out.println("incorrect");
            }
            
            System.out.println("");
        }
        
        System.out.println("Done.");
        
    }
    
    private static Boolean checkAnswer(BasicQuestion question, Integer answerResponse){
        
        QuestionType questionType = question.getQuestionType();
        Object selectedAnswer = question.getPotentialAnswers()[answerResponse - 1];
        
        if (null == questionType){
            return false;
        }else switch (questionType) {
            case MULTIPLE_CHOICE:
                return ((MultipleChoiceQuestion)question).checkAnswer(selectedAnswer.toString());
            case TRUE_FALSE:
                Boolean convertedAnswer = Boolean.parseBoolean(selectedAnswer.toString());
                return ((TrueFalseQuestion)question).checkAnswer(convertedAnswer);
            default:
                return false;
        }
        
    }
    
    private static Integer promptForAnswer(BasicQuestion question){
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter a negative number to exist.");
        System.out.print("Please enter an answer from " + "1" + " to " + Integer.toString(question.getPotentialAnswers().length ) + ": ");
        
        
        Integer result = in.nextInt();
        
        return result;
    }
    
    private static void displayQuestion(BasicQuestion question){
        
        System.out.println(question.getQuestion());
        
        String[] potentialAnswers;
        
        if (question.getQuestionType() == QuestionType.MULTIPLE_CHOICE){
            potentialAnswers = BasicQuestion.shuffleAnswers(question);
        }else{
            potentialAnswers = question.getPotentialAnswers();
        }
            
        for (int i = 0; i < potentialAnswers.length; i++){
          String optionString = String.format("%d) %s", i+1, potentialAnswers[i]);
          System.out.println(optionString);
        }
        
    }
    
}
