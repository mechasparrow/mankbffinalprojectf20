/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mechasparrow
 */
public abstract class BasicQuestion<T> {
    //have T[]
    protected String question;
    protected String category;
    protected String[] potentialAnswers;
    protected T correctAnswer;
    protected QuestionType questionType;
    protected QuestionDifficulty questionDifficulty;
    
    BasicQuestion(QuestionType questionType, String question, String[] wrongAnswers, T correctAnswer, String category, QuestionDifficulty questionDifficulty){
        this.question = question;
        this.questionType = questionType;
        this.category = category;
        this.questionDifficulty = questionDifficulty;
        this.correctAnswer = correctAnswer;
        this.potentialAnswers = concatWrongAndCorrectAnswers(wrongAnswers, correctAnswer);
        
    }
    
    public T getCorrectAnswer(){
        return this.correctAnswer;
    }
    
    public abstract boolean checkAnswer(T answer);

    public abstract boolean validAnswer(T answer); 
    
    private static String[] concatWrongAndCorrectAnswers(String[] wrongAnswers, Object correctAnswer){
        
        String correctAnswerString = correctAnswer.toString();
        
        if (wrongAnswers == null){
            return new String[]{
                correctAnswerString
            };
        }
        
        String[] combinedAnswers = new String[wrongAnswers.length + 1];
        
        for (int i = 0; i < wrongAnswers.length; i++){
            combinedAnswers[i] = wrongAnswers[i];
        }
        
        combinedAnswers[wrongAnswers.length] = correctAnswerString;
        
        return combinedAnswers;
    }
    
    public String getQuestion() {
        return this.question;
    }

    public String[] getPotentialAnswers() {
        return potentialAnswers;
    }
    
    public QuestionType getQuestionType(){
        return questionType;
    }

    
    /*journaldev.com/32661/shuffle-array-java*/
    public static String[] shuffleAnswers(BasicQuestion question){
       
        
        List<String> answerArray = Arrays.asList(question.getPotentialAnswers());
        
        Collections.shuffle(answerArray);
        
        String[] shuffledAnswers = (String[]) answerArray.toArray();
        
        return shuffledAnswers;
    }
    
}
