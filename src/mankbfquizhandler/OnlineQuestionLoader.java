/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import org.apache.commons.io.IOUtils;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.jsoup.Jsoup;

/**
 *
 * @author mechasparrow
 * 
 * Utilizes the https://opentdb.com/
 * 
 */

public class OnlineQuestionLoader extends QuestionLoader {
    
    private static final String BASE_URL = "https://opentdb.com/api.php";
    private String apiUrl;
    private static final int DEFAULT_CATEGORY = 12;
    
    private Integer questionCount;
    private Integer questionCategory;
    private QuestionType questionType;
    private QuestionDifficulty difficulty;
    
    public OnlineQuestionLoader(){
        this(DEFAULT_CATEGORY);
    }

    public OnlineQuestionLoader(Integer questionCategory){
        this(10, questionCategory, QuestionType.UNKNOWN, QuestionDifficulty.UNKNOWN);
    }
    
    public OnlineQuestionLoader(Integer questionCategory, Integer questionCount){
        this(questionCount, questionCategory, QuestionType.UNKNOWN, QuestionDifficulty.UNKNOWN);
    }
    
    public OnlineQuestionLoader(Integer questionCount, Integer questionCategory, QuestionType questionType, QuestionDifficulty difficulty){
        this.questionCount = questionCount;
        this.questionCategory = questionCategory;
        this.questionType = questionType;
        this.difficulty = difficulty;
        this.apiUrl = BASE_URL;
        updateApiUrlWithParameters();
    }
    
    private String constructQueryString(){
        //Ex: amount=10&category=10&difficulty=easy&type=boolean
        
        ArrayList<String> params = new ArrayList<>();
        
        if (questionCount != null){
            
            String amountParamString = "amount" + "=" + questionCount.toString();
            params.add(amountParamString);
            
        }
        
        if (questionCategory != null){
            String categoryParamString = "category" + "=" + questionCategory.toString();
            params.add(categoryParamString);
        }
        
        String difficultyString = QuestionMapper.difficultyToString(difficulty);
                
        if (difficultyString != null){
            
            String difficultyParamString = "difficulty" + "=" + difficultyString;
            params.add(difficultyParamString);
        }
        
        String questionTypeString = QuestionMapper.questionTypeToString(questionType);
        
        if (questionTypeString != null) {
            String typeParamString = "type" + "=" + questionTypeString;
            params.add(typeParamString);
        }
       
        String queryString = "";
        
        if (params.size() > 0){
            StringBuilder queryStringBuilder = new StringBuilder("");
        
            for (int i = 0; i < params.size(); i ++){
                if (i > 0){
                    queryStringBuilder.append("&");
                }

                queryStringBuilder.append(params.get(i));
            }
            
            queryString = queryStringBuilder.toString();
        }
        
        return queryString;
    }
    
    private void updateApiUrlWithParameters(){
        String apiUrl = BASE_URL + "?" + constructQueryString();
        this.apiUrl = apiUrl;
    }
    
    @Override
    public ArrayList<BasicQuestion> loadQuestions(){
        
        ArrayList<BasicQuestion> quizQuestions = new ArrayList<>();
        
        String rawQuestionData = loadApiData();
        
        return QuestionLoader.parseQuestionJson(rawQuestionData);
    }

    /*https://hc.apache.org/httpcomponents-client-5.0.x/index.html*/
    /*http://www.slf4j.org/*/
    private String loadApiData(){
        String data = null;
            
        try {

            CloseableHttpClient httpClient = HttpClients.createDefault();

            HttpGet httpGetRequest = new HttpGet(apiUrl);
            CloseableHttpResponse httpResponse;

            httpResponse = httpClient.execute(httpGetRequest);

            InputStream contentStream = httpResponse.getEntity().getContent();

            //https://commons.apache.org/proper/commons-io/download_io.cgi
            data = IOUtils.toString(contentStream, StandardCharsets.UTF_8.name());

            contentStream.close();
            httpResponse.close();
            httpClient.close();
        } catch (IOException ex) {
            System.out.println("Unable to retrieve url at :" + apiUrl + ": Ex: " + ex.toString());
        }

        return data;
    }
}
