/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author mechasparrow
 */
public class OfflineQuestionLoader extends QuestionLoader {

    private InputStream inputStream;
    
    private static final int DEFAULT_CATEGORY = 27;
    private int questionCategory;
    
    public OfflineQuestionLoader(){
        this(DEFAULT_CATEGORY);
    }
    
    public OfflineQuestionLoader(int questionCategory){
        this.questionCategory = questionCategory;
        loadQuestionStream();
    }
    
    private void loadQuestionStream(){
        String offlineFileName = "/mankbfquizhandler/cachedtrivia/" + Integer.toString(questionCategory) + "-" + "Questions.json";
        inputStream = OfflineQuestionLoader.class.getResourceAsStream(offlineFileName);
    }
    
    
    private String readFileToString(){
        String fileData = null;
        
        if (inputStream == null){
            return null;
        }
        
        try {
            fileData = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        } catch (FileNotFoundException ex) {
            System.out.println("Error: Unable to find file");
            System.out.println(ex.toString());
        } catch (IOException ex){
            System.out.println("Error: Unable to read text from file");
            System.out.println(ex.toString());
        }
        
        return fileData;
    }
    
    @Override
    public ArrayList<BasicQuestion> loadQuestions() {
        
        String jsonTextData = readFileToString();
        
        if (jsonTextData == null){
            return null;
        }
        
        ArrayList<BasicQuestion> questions = parseQuestionJson(jsonTextData);
        
        return questions;
    }
    
}
