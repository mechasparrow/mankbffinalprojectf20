/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mechasparrow
 */
public class MultipleChoiceQuestion extends BasicQuestion<String> {

    public MultipleChoiceQuestion(String question, String[] wrongAnswers, String correctAnswer, String category, QuestionDifficulty questionDifficulty) {
        super(QuestionType.MULTIPLE_CHOICE, question, wrongAnswers, correctAnswer, category, questionDifficulty);
    }
    
    public boolean checkAnswer (String answer) {
        return answer.equals(correctAnswer);
    }
    
    public boolean validAnswer (String answer){
        
        List<String> questionsAsList = Arrays.asList(potentialAnswers);
        
        if (questionsAsList.contains(answer)){
            return true;
        }else{
            return false;
        }
        
    }

}
