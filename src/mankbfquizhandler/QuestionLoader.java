/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

import java.io.StringReader;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.jsoup.Jsoup;

/**
 *
 * @author mechasparrow
 */
public abstract class QuestionLoader {
    
    protected static BasicQuestion parseQuestion(String category, QuestionType type, QuestionDifficulty difficulty, String question, String[] wrongAnswers, String correctAnswer){
        
        BasicQuestion parsedQuestion = null;
        
        try{
            switch (type){
                case MULTIPLE_CHOICE:
                    parsedQuestion = new MultipleChoiceQuestion(question, wrongAnswers, correctAnswer, category, difficulty);
                    break;
                case TRUE_FALSE:
                    Boolean correctBooleanAnswer = Boolean.parseBoolean(correctAnswer);
                    parsedQuestion = new TrueFalseQuestion(question, correctBooleanAnswer, category, difficulty);
                    break;
            }    
        }catch (Exception ex) {
            System.out.println("Failed to create quiz question: " + ex.toString());
        }
        
        return parsedQuestion;
    }
    
    //https://jsoup.org/
    //TODO add error checking to this
    protected static ArrayList<BasicQuestion> parseQuestionJson(String jsonText){
        ArrayList<BasicQuestion> quizQuestions = new ArrayList<>();
        
        JsonReader jsonReader = Json.createReader(new StringReader(jsonText));
        JsonObject parsedQuestionData = jsonReader.readObject();
    
        int response_code = parsedQuestionData.getJsonNumber("response_code").intValue();
        
        if (response_code == 0){
            System.out.println("Successfully retrieved Question Trivia");
            
            JsonArray jsonQuestionArray = parsedQuestionData.getJsonArray("results");
                    
            for (JsonValue jsonQuestionValue : jsonQuestionArray){
            
                JsonObject jsonObject = jsonQuestionValue.asJsonObject();
                
                String rawCategory = jsonObject.getString("category");
                
                String rawQuestionType = jsonObject.getString("type");
                QuestionType type = QuestionMapper.mapQuestionTypeFromString(rawQuestionType);
                
                String rawQuestionDifficulty = jsonObject.getString("difficulty");
                QuestionDifficulty difficulty = QuestionMapper.mapDifficultyFromString(rawQuestionDifficulty);
                
                String rawQuestion = jsonObject.getString("question");
                String rawCorrectAnswer = jsonObject.getString("correct_answer");
                
                //TODO
                JsonArray wrongJsonAnswers = jsonObject.getJsonArray("incorrect_answers");
                String[] wrongAnswers = new String[wrongJsonAnswers.size()];
                
                for (int i = 0; i < wrongAnswers.length; i++) {
                    String wrongAnswer = wrongJsonAnswers.getString(i);
                    String sanitizedWrongAnswer = Jsoup.parse(wrongAnswer).text();
                    
                    wrongAnswers[i] = sanitizedWrongAnswer;
                }
                
                String sanitizedCorrectAnswer = Jsoup.parse(rawCorrectAnswer).text();
                String sanitizedQuestion = Jsoup.parse(rawQuestion).text();
                String sanitizedCategory = Jsoup.parse(rawCategory).text();
                
                BasicQuestion quizQuestion = QuestionLoader.parseQuestion(sanitizedCategory, type, difficulty, sanitizedQuestion, wrongAnswers, sanitizedCorrectAnswer);
                if (quizQuestion != null){
                    quizQuestions.add(quizQuestion);
                }
                
            }
        }else{
            //TODO fallback to offline questions
            System.out.println("Unable to load questions from api");
        }
        
        return quizQuestions;
    }
    
    public abstract ArrayList<BasicQuestion> loadQuestions();
    
}
