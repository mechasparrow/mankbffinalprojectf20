/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

/**
 *
 * @author mechasparrow
 */
public class TrueFalseQuestion extends BasicQuestion<Boolean> {

    private static final String[] QUIZ_ANSWERS = new String[] {"True", "False"};
    
    public TrueFalseQuestion(String question, Boolean correctAnswer, String category, QuestionDifficulty questionDifficulty) {
        super(QuestionType.TRUE_FALSE, question, null , correctAnswer, category, questionDifficulty);
    }

    @Override
    public String[] getPotentialAnswers() {
        return QUIZ_ANSWERS;
    }
    
    @Override
    public boolean checkAnswer(Boolean answer) {
        return answer.equals(correctAnswer);
    }

    @Override
    public boolean validAnswer(Boolean answer) {
        return true;
    }
    
}
