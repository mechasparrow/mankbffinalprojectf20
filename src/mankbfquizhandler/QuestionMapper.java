/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbfquizhandler;

/**
 *
 * @author mechasparrow
 */
public abstract class QuestionMapper {
    
    public static QuestionDifficulty mapDifficultyFromString(String difficulty){
        QuestionDifficulty questionDifficulty = QuestionDifficulty.UNKNOWN;
        
        switch(difficulty){
            case "easy":
                questionDifficulty = QuestionDifficulty.EASY;
                break;
            case "medium":
                questionDifficulty = QuestionDifficulty.MEDIUM;
                break;
            case "hard":
                questionDifficulty = QuestionDifficulty.HARD;
                break;
        }
     
        return questionDifficulty;
    }
    
    public static String difficultyToString(QuestionDifficulty questionDifficulty){
        String difficultyString = null;
        
        switch (questionDifficulty){
            case EASY:
                difficultyString = "easy";
                break;
            case MEDIUM:
                difficultyString = "medium";
                break;
            case HARD:
                difficultyString = "hard";
                break;
        }
        
        return difficultyString;
    }
    
    public static String questionTypeToString(QuestionType questionType){
        String questionTypeString = null;
        
        switch (questionType){
            case MULTIPLE_CHOICE:
                questionTypeString = "multiple";
                break;
            case TRUE_FALSE:
                questionTypeString = "boolean";
                break;
        }
        
        return questionTypeString;
    }
    
    public static QuestionType mapQuestionTypeFromString(String type){
        QuestionType questionType = QuestionType.UNKNOWN;
        
        switch(type){
            case "multiple":
                questionType = QuestionType.MULTIPLE_CHOICE;
                break;
            case "boolean":
                questionType = QuestionType.TRUE_FALSE;
                break;
        }
        
        return questionType;
    }
    
}
