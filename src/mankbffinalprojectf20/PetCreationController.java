/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author mechasparrow
 */
public class PetCreationController extends Switchable implements Initializable, PropertyChangeListener, PetModelUtilizable {

    
    @FXML
    private Label petCreationLabel;
    @FXML
    private Button cancelButton;
    
    @FXML
    private ChoiceBox<String> headChoiceSelector;
    @FXML
    private ChoiceBox<String> bodyChoiceSelector;
    @FXML
    private ChoiceBox<String> tailChoiceSelector;
    @FXML
    private ChoiceBox<String> legChoiceSelector;
    
    @FXML
    private TextField petTextField;
    @FXML
    private ChoiceBox<Gender> genderSelector;
    @FXML
    private Button petCreateButton;
    @FXML
    private Text errorText;
    
    private HashMap<PetBodyPart, ChoiceBox<String>> bodyChoiceSelectors;
    private HashMap<PetBodyPart, HashMap<String, String>> bodyChoiceValues;
    
    private static final String[] RANDOM_NAMES = new String[] {
            "Paige" ,
            "Abdi" ,
            "Niall" ,
            "Reid" ,
            "Havin" ,
            "Astrid" ,
            "Aimie" ,
            "Kelsie" ,
            "Malak" ,
            "Caprice"
    };
    
    @FXML
    private StackPane characterPane;
    
    private PetModel petModel;
    private PetUI petUserInterface;
    @FXML
    private Button errorButton;
    
    /**
     * TODO add option to reset form
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initializeLookupTables();
        
        petUserInterface = new PetUI(characterPane);
        petUserInterface.switchMode("Horizontal");
        
        for (PetBodyPart petBodyPart : PetBodyPart.values()){
            HashMap<String, String> availableChoices = bodyChoiceValues.get(petBodyPart);
            ChoiceBox<String> choiceSelector = bodyChoiceSelectors.get(petBodyPart);

            System.out.println(choiceSelector);
            
            choiceSelector.setItems(
                    FXCollections.observableArrayList(availableChoices.keySet())
            );
            
            choiceSelector.getSelectionModel().select("Generic");
            
            choiceSelector.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue ov, Number value, Number new_value) {
                    handlePetAttributeSelection(petBodyPart, new_value);
                }
            });
        }
        
        genderSelector.getItems().setAll(Gender.values());
        genderSelector.setValue(Gender.MALE);
        
    }
    
    private void initializeLookupTables(){
        bodyChoiceSelectors = new HashMap<PetBodyPart, ChoiceBox<String>>(){{
            put(PetBodyPart.HEAD, headChoiceSelector);
            put(PetBodyPart.BODY, bodyChoiceSelector);
            put(PetBodyPart.TAIL, tailChoiceSelector);
            put(PetBodyPart.LEGS, legChoiceSelector);  
        }};

        bodyChoiceValues = new HashMap<PetBodyPart, HashMap<String, String>>(){{
            put(
                PetBodyPart.HEAD, 
                new HashMap<String, String>(){{
                    put("Generic", "generic");
                    put("Cat", "cat");
                    put("Panda", "panda");
                    put("Zebra", "zebra");
                }}
            );

            put(
                PetBodyPart.BODY, 
                new HashMap<String, String>(){{
                    put("Generic", "generic");
                    put("Cat", "cat");
                    put("Zebra", "zebra");
                    put("Panda", "panda");
                }}
            );

            put(
                PetBodyPart.LEGS, 
                new HashMap<String, String>(){{
                    put("Generic", "generic");
                    put("Cat", "cat");
                    put("Zebra", "zebra");
                    put("Panda", "panda");
                }}
            );

            put(
                PetBodyPart.TAIL, 
                new HashMap<String, String>(){{
                    put("Generic", "generic");
                    put("Cat", "cat");
                    put("Panda", "panda");
                    put("Zebra", "zebra");
                }}
            );
        }};
    }
    
    private void updatePetBodyPart(PetBodyPart petBodyPart, String bodyPartName){
        if (petBodyPart != null){
            petModel.updatePetBodyPart(petBodyPart, bodyPartName);
        }    
    } 
    
    private void handlePetAttributeSelection(PetBodyPart petBodyPart, Number new_value){
        ChoiceBox <String> selector = bodyChoiceSelectors.get(petBodyPart);
        HashMap<String, String> partNameChoices = bodyChoiceValues.get(petBodyPart);
        
        String selectedValue = selector.getItems().get(new_value.intValue());
        String bodyPartName = partNameChoices.get(selectedValue);
        updatePetBodyPart(petBodyPart, bodyPartName);
    }
    
    @Override
    public void setPetModel(PetModel petModel){
        if (this.petModel != null){
            this.petModel.removePropertyChangeListener(this);
            this.petModel.removePropertyChangeListener(petUserInterface);
            this.petModel = null;
        }
        
        this.petModel = petModel;
        this.petModel.addPropertyChangeListener(this);
        this.petModel.addPropertyChangeListener(petUserInterface);
        this.petModel.refreshPetInformation();
    }

    private void updateCreationLabel(int petNumber){
        petCreationLabel.setText("Creating Pet " + petNumber);
    }
    
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.println(evt);
        String eventName = evt.getPropertyName();
        
        if (eventName.equals("PetNumber")){
            int petNumber = (Integer) evt.getNewValue();
            updateCreationLabel(petNumber);
        }
        
        if (eventName.equals("Error")){
            String errorToDisplay = evt.getNewValue().toString();
            raiseError(errorToDisplay);
        }else if (eventName.equals("Success")){
            clearError();
            
            //TODO switch to game
            openGame();
        }
    }

    private void openGame(){
        
        boolean petExists = petModel != null;
        
        Switchable sceneController = null;
        
        if (petExists){
            System.out.println("Existing pet");

            Switchable.remove("PetCreationView");
            Switchable.switchTo("MainGameView");
        
            sceneController = Switchable.getControllerByName("MainGameView");
        }
        
        if (sceneController != null && sceneController instanceof PetModelUtilizable){
            PetModelUtilizable petCreationController = (PetModelUtilizable) sceneController;
            petCreationController.setPetModel(petModel);
        }
    }
    
    @FXML
    private void onCancel(ActionEvent event) {
        if (petModel != null){
            petModel.removePropertyChangeListener(this);
            petModel.removePropertyChangeListener(petUserInterface);
            petModel = null;
        }
        
        Switchable.remove("PetCreationView");
        Switchable.switchTo("MainMenuView");
    }


    private void raiseError(String errorTextToDisplay){
        errorText.setVisible(true);
        errorButton.setVisible(true);
        errorText.setText(errorTextToDisplay);
    }
    
    private void clearError(){
        errorText.setVisible(false);
        errorButton.setVisible(false);
    }
    
    @FXML
    private void onPetCreate(ActionEvent event) {
        Gender petGender = genderSelector.getValue();
        String petName = petTextField.getText();
        
        for (PetBodyPart petBodyPart : PetBodyPart.values())
        {
            HashMap<String, String> availableChoices = bodyChoiceValues.get(petBodyPart);
            ChoiceBox<String> choiceSelector = bodyChoiceSelectors.get(petBodyPart);
            String selectedBodyPartName = availableChoices.get(choiceSelector.getValue());
            
            updatePetBodyPart(petBodyPart, selectedBodyPartName);
        }
        
        petModel.updatePet(petGender, petName);
    }

    @FXML
    private void onPetDefault(ActionEvent event) {
        genderSelector.setValue(Gender.MALE);
        
        
        Random random = new Random(System.currentTimeMillis());
        
        String randomName = RANDOM_NAMES[random.nextInt(RANDOM_NAMES.length)];
        
        petTextField.setText(randomName);
        
        for (PetBodyPart petBodyPart : PetBodyPart.values()){
            ChoiceBox<String> choiceSelector = bodyChoiceSelectors.get(petBodyPart);
            int choicesCount = choiceSelector.getItems().size();
            int selectionIdx = random.nextInt(choicesCount);
            choiceSelector.getSelectionModel().select("Generic");
        }
        
        onPetCreate(event);
    }

    @FXML
    private void closeError(ActionEvent event) {
        clearError();
    }

    
}
