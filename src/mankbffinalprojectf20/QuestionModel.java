/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.util.ArrayList;
import java.util.Random;
import javafx.application.Platform;
import mankbfquizhandler.BasicQuestion;
import mankbfquizhandler.OfflineQuestionLoader;
import mankbfquizhandler.QuestionLoader;
import mankbfquizhandler.OnlineQuestionLoader;
import mankbfquizhandler.TrueFalseQuestion;

/**
 *
 * @author mechasparrow
 */
public class QuestionModel extends AbstractModel {
    
    private boolean trueFalseMode = false;
    private Boolean questionCorrect = null;
    private BasicQuestion question;
    
    private static ArrayList<BasicQuestion> questionsCache = null;
    
    private QuestionLoader questionLoader;
    
    QuestionModel(){
    }
    
    private void loadOnlineQuestionsAsync(){
        Thread onlineQuestionsThread = new Thread(() -> {
                questionLoader = new OnlineQuestionLoader(27,50);
                
                ArrayList<BasicQuestion> questions = questionLoader.loadQuestions();
                
                if (questions != null){
                    Platform.runLater(() -> {
                        questionsCache = questions;
                        System.out.println("Online questions loaded");
                    });
                }
            });
            
            onlineQuestionsThread.start();
    }
    
    public void loadQuestion(){
        if (questionsCache == null){
            questionLoader = new OfflineQuestionLoader();
            questionsCache = questionLoader.loadQuestions();
            
            loadOnlineQuestionsAsync();
        }
        
        Random random = new Random();
        BasicQuestion question = questionsCache.get(random.nextInt(questionsCache.size()));
        
        if (question instanceof TrueFalseQuestion){
            toggleTrueFalseMode();
        }
        
        this.question = question;
        
        super.firePropertyChange("DISPLAY_QUESTION", this.trueFalseMode, this.question);
    }
    
    private void toggleTrueFalseMode(){
        trueFalseMode = true;
        
        super.firePropertyChange("TRUE_FALSE_MODE", null, trueFalseMode);
        
    }
    
    public Boolean getQuestionCorrect(){
        return questionCorrect;
    }

    public void submitAnswer(Object userAnswer) {
        if (question == null){
            return;
        }
        
        Boolean questionCorrect = question.checkAnswer(userAnswer);
        
        if (questionCorrect != null){
            this.questionCorrect = questionCorrect;
        }
        
        super.firePropertyChange("QUESTION_SUBMITED", question, questionCorrect);
    }
}
