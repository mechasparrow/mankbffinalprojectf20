/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import static mankbffinalprojectf20.Switchable.stage;
import mankbfquizhandler.BasicQuestion;
import mankbfquizhandler.OfflineQuestionLoader;
import mankbfquizhandler.QuestionLoader;
import mankbfquizhandler.OnlineQuestionLoader;
import mankbfquizhandler.TrueFalseQuestion;

/**
 * FXML Controller class
 *
 * @author mechasparrow
 */
public class AskQuestionController implements Initializable, PropertyChangeListener {

    @FXML
    private Label questionLabel;
    @FXML
    private ToggleGroup questionGroup;
    @FXML
    private RadioButton radioOption1;
    @FXML
    private RadioButton radioOption2;
    @FXML
    private RadioButton radioOption3;
    @FXML
    private RadioButton radioOption4;

    
    private Stage stage;
    
    private QuestionModel questionModel;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        questionModel = new QuestionModel();
        questionModel.addPropertyChangeListener(this);
        questionModel.loadQuestion();
        
    }    
    
    public void setStage(Stage stage){
        this.stage = stage;
    }

    private void displayQuestion(BasicQuestion question, boolean trueFalseMode){
        System.out.println("Display");
        String[] potentialAnswers = question.getPotentialAnswers();
        
        
        
        if (trueFalseMode == false){
            String[] potentialAnswersShuffled = BasicQuestion.shuffleAnswers(question);
            
            radioOption1.setText(potentialAnswersShuffled[0]);
            radioOption2.setText(potentialAnswersShuffled[1]);
            radioOption3.setText(potentialAnswersShuffled[2]);
            radioOption4.setText(potentialAnswersShuffled[3]);
        
            radioOption1.setUserData(potentialAnswersShuffled[0]);
            radioOption2.setUserData(potentialAnswersShuffled[1]);
            radioOption3.setUserData(potentialAnswersShuffled[2]);
            radioOption4.setUserData(potentialAnswersShuffled[3]);
        }else{
            radioOption1.setText(potentialAnswers[0]);
            radioOption2.setText(potentialAnswers[1]);
            radioOption1.setUserData(Boolean.parseBoolean(potentialAnswers[0]));
            radioOption2.setUserData(Boolean.parseBoolean(potentialAnswers[1]));
        }
        
        
        
        questionLabel.setText(question.getQuestion());
        
    }
    
    public QuestionModel getQuestionModel(){
        return this.questionModel;
    }
    
    @FXML
    private void onQuestionSkip(ActionEvent event) {
        showAnswerDialog(null, null);
        closeDialog();
    }

    private void closeDialog(){
        if (stage != null){
            stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));   
            questionModel.removePropertyChangeListener(this);
        }
    }
    
    @FXML
    private void onQuestionSubmission(ActionEvent event) {
        Toggle selectedToggle = questionGroup.getSelectedToggle();
        
        Boolean questionCorrect = null;
        
        if (selectedToggle != null){
            Object userAnswer = selectedToggle.getUserData();
            
            questionModel.submitAnswer(userAnswer);
        }
        
    }
    
    //TODO show correct answer dialog
    
    private void showAnswerDialog(BasicQuestion question, Boolean questionCorrect){
        Alert answerAlert = new Alert(AlertType.INFORMATION);
        
        if (question != null && questionCorrect != null){
            Image image = null;
            
            if (questionCorrect == true){
                image = new Image(AskQuestionController.class.getResourceAsStream("/mankbffinalprojectf20/petassets/correct.png"));
            
                answerAlert.setHeaderText("Correct");
                answerAlert.setTitle("Answer Correct");
        
            }else if (questionCorrect == false){
                image = new Image(AskQuestionController.class.getResourceAsStream("/mankbffinalprojectf20/petassets/incorrect.png"));
                
                answerAlert.setHeaderText("Incorrect");
                answerAlert.setTitle("Answer Incorrect");
                
                Label correctAnswerLabel = new Label("The correct answer was " + "\"" + question.getCorrectAnswer().toString() + "\"");
                correctAnswerLabel.setMinWidth(400);
                answerAlert.getDialogPane().setContent(correctAnswerLabel);
                
            }

            ImageView imageView = new ImageView(image);
            answerAlert.setGraphic(imageView);
        }else
        {
            answerAlert.setHeaderText("Question skipped");
            answerAlert.setTitle("Question was skipped");
        }
        
        answerAlert.showAndWait();  
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String eventName = evt.getPropertyName();
        
        if (eventName.equals("DISPLAY_QUESTION")){
            System.out.println("display");
            BasicQuestion question = (BasicQuestion) evt.getNewValue();
            boolean trueFalseMode = (boolean)evt.getOldValue();
            
            displayQuestion(question, trueFalseMode);
        }
        
        else if (eventName.equals("TRUE_FALSE_MODE")){
            boolean trueFalseMode = (boolean)evt.getNewValue();
            
            if (trueFalseMode){
                radioOption3.setVisible(false);
                radioOption4.setVisible(false);
            }
        }
        
        else if (eventName.equals("QUESTION_SUBMITED")){
            Boolean questionCorrect = (Boolean)evt.getNewValue();
            BasicQuestion question = (BasicQuestion) evt.getOldValue();
            showAnswerDialog(question,questionCorrect);
            closeDialog();
        }
        
    }
    
    
}
