/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author mechasparrow
 */
public class MainMenuController extends Switchable implements Initializable {
    
    //TODO add option to delete animals
    
    private Label label;
    
    @FXML
    private Button startPet1;
    @FXML
    private Button startPet2;
    @FXML
    private Button startPet3;
    
    @FXML
    private Button helpButton;
    @FXML
    private Button exitButton;
    @FXML
    private Button aboutButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        Switchable.stage.setOnCloseRequest(wc -> {
            System.out.println("Closing");
        });
    }    

    private void handlePetLoad(int petNum){
        
        PetModel petModel = PetModel.loadPetModel(petNum);
        
        boolean petExists = petModel != null;
        
        Switchable sceneController = null;
        
        if (petExists){
            System.out.println("Existing pet");

            Switchable.switchTo("MainGameView");
        
            sceneController = Switchable.getControllerByName("MainGameView");
        }
        else {
            petModel = new PetModel(petNum);
            
            Switchable.switchTo("PetCreationView");
            
            sceneController = Switchable.getControllerByName("PetCreationView");
            
        }
        
        if (sceneController instanceof PetModelUtilizable){
            PetModelUtilizable petCreationController = (PetModelUtilizable) sceneController;
            petCreationController.setPetModel(petModel);
        }
        
    }
    
    @FXML
    private void handleLoadPet1(ActionEvent event) {
        handlePetLoad(1);
    }

    @FXML
    private void handleLoadPet2(ActionEvent event) {
        handlePetLoad(2);
    }

    @FXML
    private void handleLoadPet3(ActionEvent event) {
        handlePetLoad(3);
    }

    @FXML
    private void onHelpButton(ActionEvent event) {
        //TODO
        Switchable.switchTo("HelpView");
    }

    @FXML
    private void onExitButton(ActionEvent event) {
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    @FXML
    private void onAboutButton(ActionEvent event) {
        //TODO
        Switchable.switchTo("AboutView");
    }
    
}
