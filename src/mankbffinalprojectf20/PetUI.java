/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 *
 * @author mechasparrow
 */
public class PetUI implements PropertyChangeListener {
    
    private StackPane stackPane;
    
    private HashMap<PetBodyPart, ImageView[]> petBodyPartImageViews;
    private HashMap<PetBodyPart, Double[]> petPartOffsets;
    private HashMap<PetBodyPart, Double[]> petPartOffsetsFourLegs;
    private HashMap<PetBodyPart, Double[]> petPartScales;
    
    private String mode;
    
    PetUI(StackPane stackPane){
        this(stackPane, "Vertical");
    }
    
    PetUI(StackPane stackPane, String mode){
        this.stackPane = stackPane;
        this.mode = mode;
        loadLookupTables();
        initializeImages();
    }
    
    private void loadLookupTables(){
        petPartOffsets = new HashMap<PetBodyPart, Double[]>(){{
                put(PetBodyPart.HEAD, new Double[]{0.0, 0.0 - 70});
                put(PetBodyPart.BODY, new Double[]{0.0, 70.0 - 70});
                put(PetBodyPart.LEGS, new Double[]{0.0, 118.0 - 70 });
                put(PetBodyPart.TAIL, new Double[]{-50.0,80.0 - 70});
        }};

        petPartOffsetsFourLegs = new HashMap<PetBodyPart, Double[]>(){{
           put(PetBodyPart.HEAD, new Double[]{50.0, -20.0});
           put(PetBodyPart.BODY, new Double[]{0.0, 70.0 - 70});
           put(PetBodyPart.LEGS, new Double[]{25.0, 100.0 - 70, -45.0});
           put(PetBodyPart.TAIL, new Double[]{-70.0,60.0 - 70});
        }};
        petPartScales = new HashMap<PetBodyPart, Double[]>(){{
           put(PetBodyPart.HEAD, new Double[]{0.4, 0.4});
           put(PetBodyPart.BODY, new Double[]{0.4, 0.4});
           put(PetBodyPart.LEGS, new Double[]{0.325, 0.4});
           put(PetBodyPart.TAIL, new Double[]{0.35, 0.35});
        }};
    }
    
    public void switchMode(String newMode){
        this.mode = newMode;
        
        stackPane.getChildren().clear();
        
        initializeImages();
    }
    
    private void initializeImages(){
        petBodyPartImageViews = new HashMap<>();
        HashMap<PetBodyPart, Double[]> petBodyPartOffsets;
        
        if (mode.equals("Vertical")){
            petBodyPartOffsets = petPartOffsets;
        }else{
            petBodyPartOffsets = petPartOffsetsFourLegs;
        }
        
        for (int i = PetBodyPart.values().length - 1; i >= 0; i--){
            PetBodyPart bodyPart = PetBodyPart.values()[i];
        
            String imageUrl = "generic-pet-" + bodyPart.toString().toLowerCase() + ".png";
            
            Image image = new Image(
                    PetUI.class.getResourceAsStream("/mankbffinalprojectf20/petassets/" + imageUrl)
            );
            
            ImageView imageViews[];
            
            if (bodyPart == PetBodyPart.LEGS && !mode.equals("Vertical")){
                imageViews = new ImageView[2];
            }else{
                imageViews = new ImageView[1];
            }
            
            for (int c = 0; c < imageViews.length; c++){
                ImageView imageView = new ImageView();
            
                imageView.setImage(image);
            
                Double[] scales = petPartScales.get(bodyPart);
                imageView.setScaleX(scales[0]);
                imageView.setScaleY(scales[1]);
                
                Double[] offsets = petBodyPartOffsets.get(bodyPart);
                
                imageView.setTranslateX(offsets[0]);
                
                if (bodyPart == PetBodyPart.LEGS && c == 1){
                    imageView.setTranslateX(imageView.getTranslateX() + offsets[2]);
                }
                
                imageView.setTranslateY(offsets[1]);
            
                if (bodyPart == PetBodyPart.BODY && !mode.equals("Vertical")){
                    imageView.setRotate(90);
                }
            
                imageViews[c] = imageView;
            }
            
            petBodyPartImageViews.put(bodyPart, imageViews);
            
            for (ImageView imageView : imageViews){
                stackPane.getChildren().add(imageView);
            }
        }
    }
    
    private void displayUpdatedBodyPart(PetBodyPart bodyPart, String updatedBodyPartImageUrl) {
        
        if (bodyPart != null && updatedBodyPartImageUrl != null){
            
            Image image = new Image(
                    PetUI.class.getResourceAsStream("/mankbffinalprojectf20/petassets/" + updatedBodyPartImageUrl)
            );

            ImageView[] imageViewsToUpdate = null;
            
            imageViewsToUpdate = petBodyPartImageViews.get(bodyPart);
            
            if (imageViewsToUpdate != null){
                for (ImageView imageViewToUpdate : imageViewsToUpdate){
                    imageViewToUpdate.setImage(image);
                }
            }
            
        }
        
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String eventName = evt.getPropertyName();
        String bodyPartChangedSuffix = "-body-part-changed";
        
        for (PetBodyPart bodyPart : PetBodyPart.values()){
            if (eventName.equals(bodyPart.toString() + bodyPartChangedSuffix)){
                String updatedBodyPartImageUrl = evt.getNewValue().toString();
                displayUpdatedBodyPart(bodyPart, updatedBodyPartImageUrl );
            }
        }
    }
        
}
