/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author mechasparrow
 */
public class WorldModel extends AbstractModel {
    
    private double worldTime;
    private int daysPassed;
    private int actionTokens;
            
    private Timeline worldTimeline;
    private Timeline activityTimeline;
    
    private double timelineSpeed = 100;
    
    private double tickElapsed = 0;
    private double tickThreshold = Duration.hours(6).toMillis();
    
    private double timeResolution = 10000.0;
    
    private boolean worldDestroyed = false;
    
    WorldModel(){
        super();
        this.worldTime = 0;
        this.daysPassed = 1;
        this.actionTokens = 0;
        initTimeline();
        activityTimeline = null;
    }
    
    public void refreshWorldInformation(){
        super.firePropertyChange("DAY_PASSED", null, daysPassed);
        super.firePropertyChange("WORLD_TIME", null, getTimeString(worldTime));
        super.firePropertyChange("TOKEN_CHANGE", null ,actionTokens);
    }
    
    public void questionAnswered(Boolean correct){
        
        if (correct != null){
            if (correct == true){
                addTokens(20);
            }else{
                addTokens(15);
            }
        }else{
            addTokens(5);
        }
        
        
    }
    
    public void addTokens(int tokenGain){
        actionTokens += tokenGain;
        System.out.println("Token change");
        System.out.println(actionTokens);
        super.firePropertyChange("TOKEN_CHANGE", null ,actionTokens);
    }
    
    public boolean payTokens(int tokenCost){
        
        if (canIncurTokenCost(tokenCost)){
            actionTokens -= tokenCost;
            super.firePropertyChange("TOKEN_CHANGE", null ,actionTokens);
            return true;
        }else{
            super.firePropertyChange("ERROR", null ,"Insufficient Tokens");
            return false;
        }
    }
    
    private boolean canIncurTokenCost(int tokenCost){
        return (actionTokens - tokenCost) >= 0;
    }
    
    private void initActivityTimeline(String activity){
        if (activityTimeline != null){
            activityTimeline.stop();
            activityTimeline = null;
        }
        
        super.firePropertyChange("ACTIVITY_START", null, activity);
        activityTimeline = new Timeline (new KeyFrame(Duration.millis(1000), (evt) -> {
            
            
        }));
        
        activityTimeline.setOnFinished((evt) -> {
            super.firePropertyChange("ACTIVITY_END ", null, activity);
        });
        
        activityTimeline.setCycleCount(1);
        activityTimeline.play();
    }
    
    private void initTimeline(){
        worldTimeline = new Timeline(new KeyFrame(Duration.millis(timelineSpeed), (evt) ->{
            handleWorldUpdate(evt);
        }));
        
        worldTimeline.setCycleCount(Timeline.INDEFINITE);
        
    }
    
    public void startWorldActivity(String activity){
        initActivityTimeline(activity);
    }
    
    public void startWorld(){
        worldTimeline.play();
    }
    
    public void stopWorld(){
        worldTimeline.pause();
    }
    
    public boolean isWorldDestroyed(){
        return worldDestroyed;
    }
    
    public void destroyWorld(int worldId){
        if (worldDestroyed){
            return;
        }
        
        worldTimeline.stop();
        worldTimeline = null;
    
        if (activityTimeline != null){
            activityTimeline.stop();
            activityTimeline = null;
        }
        
        worldDestroyed = true;
        
        String worldFileName = "worldSave" + Integer.toString(worldId) + ".json";
        String fileFolderPath = System.getProperty("user.dir") + "/Worlds/";
        File petFile = new File(fileFolderPath + worldFileName);
            
        boolean fileExists = petFile.exists();
        boolean worldDeleted = false;
        
        if (fileExists){
            worldDeleted = petFile.delete();
        }
        
        worldDestroyed = true;
    }
    
    private void handleWorldUpdate(ActionEvent evt){
        worldTime += timelineSpeed * timeResolution;
        tickElapsed += timelineSpeed * timeResolution;
        
        Duration duration = Duration.millis(worldTime);
        if (duration.toHours() >= 24){
            worldTime = 0;
            daysPassed++;
            super.firePropertyChange("DAY_PASSED", null, daysPassed);
            super.firePropertyChange("WORLD_AUTOSAVE", null, 1);
        }
        
        if (tickElapsed >= tickThreshold){
            super.firePropertyChange("WORLD_TICK", null, true);
            tickElapsed = 0;
        }
        
        super.firePropertyChange("WORLD_TIME", null, getTimeString(worldTime));
    }
    
    public void saveWorld(int worldId){
        String serializedWorldData = null;
        
        HashMap<String, Object> worldHashMap = new HashMap<>();
        
        worldHashMap.put("worldTime", worldTime);
        worldHashMap.put("daysPassed", daysPassed);
        worldHashMap.put("actionTokens", actionTokens);
        worldHashMap.put("timelineSpeed", timelineSpeed);
        worldHashMap.put("tickThreshold", tickThreshold);
        worldHashMap.put("timeResolution", timeResolution);
        
        JsonObject worldJsonModel = Json.createObjectBuilder(worldHashMap).build();
        serializedWorldData = worldJsonModel.toString();
        
        
        FileWriter fileWriter = null;
        String worldFileName = "worldSave" + Integer.toString(worldId) + ".json";
        String fileFolderPath = System.getProperty("user.dir") + "/Worlds/";
        File fileFolder = new File(fileFolderPath);

        try {
            
            boolean folderExists = fileFolder.exists();
            
            if (!folderExists){
                boolean folderCreated = fileFolder.mkdir();
                if (folderCreated){
                    System.out.println("World folder successfully created");
                }else{
                    System.out.println("Err: Unable to create World Folder");
                }
            }   
            
            File worldFile = new File(fileFolderPath + worldFileName);
            
            boolean fileExists = worldFile.exists();
            
            if (!fileExists){
                boolean fileCreated = worldFile.createNewFile();
                if (fileCreated){
                    System.out.println("World " + worldFile + " file created!");
                }else{
                    System.out.println("Unable to create " + worldFile + " file.");
                }
            }   
            
            fileWriter = new FileWriter(worldFile);            
            fileWriter.write(serializedWorldData);
            System.out.println("Saving World to " + worldFileName);
        
        } catch (IOException ex) {
            Logger.getLogger(WorldModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(WorldModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static WorldModel loadWorld(int worldId){
        WorldModel parsedWorldModel = new WorldModel();
        
        String worldFileName = "worldSave" + Integer.toString(worldId) + ".json";
        String fileFolderPath = System.getProperty("user.dir") + "/Worlds/";
        File worldFile = new File(fileFolderPath + worldFileName);
            
        boolean fileExists = worldFile.exists();

        if (fileExists){
            
            FileInputStream fileInputStream = null;
            
            try {
                fileInputStream = new FileInputStream(worldFile);
                
                String fileData = IOUtils.toString(fileInputStream, StandardCharsets.UTF_8.name());
            
                JsonReader jsonReader = Json.createReader(new StringReader(fileData));
                JsonObject worldObject = jsonReader.readObject();
                
              
                if (worldObject.containsKey("worldTime")){
                    double worldTime = worldObject.getJsonNumber("worldTime").doubleValue();
                    parsedWorldModel.setWorldTime(worldTime);
                }
                
                if (worldObject.containsKey("daysPassed")){
                    int daysPassed = worldObject.getJsonNumber("daysPassed").intValue();
                    parsedWorldModel.setDaysPassed(daysPassed);
                }
                
                if (worldObject.containsKey("actionTokens")){
                    int actionTokens = worldObject.getJsonNumber("actionTokens").intValue();
                    parsedWorldModel.setActionTokens(actionTokens);
                }
                
                if (worldObject.containsKey("timelineSpeed")){
                    double timelineSpeed = worldObject.getJsonNumber("timelineSpeed").doubleValue();
                    parsedWorldModel.setTimelineSpeed(timelineSpeed);
                }
                
                if (worldObject.containsKey("tickThreshold")){
                    double tickThreshold = worldObject.getJsonNumber("tickThreshold").doubleValue();
                    parsedWorldModel.setTickThreshold(tickThreshold);
                }
                
                if (worldObject.containsKey("timeResolution")){
                    double timeResolution = worldObject.getJsonNumber("timeResolution").doubleValue();
                    parsedWorldModel.setTimeResolution(timeResolution);
                }
                
            }catch (JsonParsingException ex){
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try{
                    fileInputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            
            
        }else{
            System.out.println("Unable to load world " + worldId);
        }
        
        System.out.println("Retrieving World " + worldFileName);

        parsedWorldModel.logInformation();
        return parsedWorldModel;
        
    }
    
    private static String getTimeString(Double time){
        
        Duration duration = Duration.millis(time);
        
        int hours = (int)duration.toHours();
        int minutes = (int)(duration.toMinutes() - (hours * 60));
        int seconds = (int)(duration.toSeconds() - ((int)duration.toMinutes() * 60));
        
        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        return timeString;
    }

    public void setWorldTime(double worldTime) {
        this.worldTime = worldTime;
        super.firePropertyChange("WORLD_TIME", null, getTimeString(worldTime));
    }

    public void setDaysPassed(int daysPassed) {
        this.daysPassed = daysPassed;
        super.firePropertyChange("DAY_PASSED", null, daysPassed);
    }

    public void setActionTokens(int actionTokens) {
        this.actionTokens = actionTokens;
        super.firePropertyChange("TOKEN_CHANGE", null ,actionTokens);
    }

    public void setTimelineSpeed(double timelineSpeed) {
        this.worldTimeline.stop();
        this.worldTimeline = null;
        
        this.timelineSpeed = timelineSpeed;
        
        
        initTimeline();
        
    }

    public void setTickThreshold(double tickThreshold) {
        this.tickThreshold = tickThreshold;
    }

    public void setTimeResolution(double timeResolution) {
        this.timeResolution = timeResolution;
    }

    public void logInformation() {
        System.out.println("Tick Threshold: " + Double.toString(tickThreshold));
        
        System.out.println("Time Resolution: " + Double.toString(timeResolution));
        
        System.out.println("Timeline Speed: " + Double.toString(timelineSpeed));
        
        System.out.println("World Time: " + Double.toString(worldTime));
        
        System.out.println("Action Tokens: " + Integer.toString(actionTokens));
        
        
        System.out.println("Days Passed: " + Integer.toString(daysPassed));
    }

    public int getDays() {
        return this.daysPassed;
    }

    
}
