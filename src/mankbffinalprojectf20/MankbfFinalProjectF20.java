/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import javafx.application.Application;
import static javafx.application.Application.STYLESHEET_MODENA;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author mechasparrow
 */
public class MankbfFinalProjectF20 extends Application {
    
    private static final String APPLICATION_NAME = "Petastic Pals";
    
    @Override
    public void start(Stage stage) throws Exception {
        HBox root = new HBox();
        
        root.setPrefSize(854, 480);
        
        root.setAlignment(Pos.CENTER);
        
        Text message = new Text("Unable to locate scene!");
       
        message.setFont(Font.font(STYLESHEET_MODENA, 32));
        
        root.getChildren().add(message);
        
        // create Scene and init UI to show failure in case switch fails
        Scene scene = new Scene(root);
        
        Switchable.scene = scene;
        Switchable.stage = stage;
        
        Switchable.switchTo("MainMenuView");
        
        stage.setTitle(APPLICATION_NAME);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
