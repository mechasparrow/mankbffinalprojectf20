/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author mechasparrow
 */
public class MainGameController extends Switchable implements Initializable,PropertyChangeListener, PetModelUtilizable{

    private PetModel petModel;
    private WorldModel worldModel;
    private PetUI petUserInterface;
    
    @FXML
    private StackPane characterPane;
    
    @FXML
    private Button backButton;
    @FXML
    private Text timeText;
    @FXML
    private Text tokensText;
    @FXML
    private Label playPetCostLabel;
    @FXML
    private Label feedPetCostLabel;
    @FXML
    private Label waterPetCostLabel;
    @FXML
    private Label errorText;
    @FXML
    private ProgressBar hungerProgress;
    @FXML
    private ProgressBar thirstProgress;
    @FXML
    private ProgressBar playProgress;
    @FXML
    private Text petNameText;
    @FXML
    private Text petGenderText;
    @FXML
    private Text daysText;
    @FXML
    private Button errorOkButton;
    @FXML
    private ImageView activityImageView;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        petUserInterface = new PetUI(characterPane);
        
        
        loadInWorld();
        
        clearError();
        
        Switchable.stage.setOnCloseRequest(wc -> {
           goBackToMainMenu(null);
        });
    }    

    private void clearError(){
        errorText.setText("");
        errorText.setVisible(false);
        errorOkButton.setVisible(false);
    }
    
    private void showError(String errorMessage){
        errorText.setText(errorMessage);
        errorText.setVisible(true);
        errorOkButton.setVisible(true);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String eventName = evt.getPropertyName();
        Object source = evt.getSource();
        
        if (source instanceof WorldModel){
            if (eventName.equals("WORLD_TIME")){
                String worldTimeString = evt.getNewValue().toString();
                timeText.setText(worldTimeString);
            }else if (eventName.equals("DAY_PASSED")){
                String dayString = evt.getNewValue().toString();
                daysText.setText(dayString);
                
            }else if (eventName.equals("WORLD_AUTOSAVE")){
                saveWorldAndPet();
            }else if (eventName.equals("TOKEN_CHANGE")){
                String actionTokensString = evt.getNewValue().toString();
                tokensText.setText(actionTokensString);
            }else if (eventName.equals("WORLD_TICK")){
                if (petModel != null){
                    petModel.worldTick();
                }
            }
            
            
            else if (eventName.startsWith("ACTIVITY")){
                System.out.println(eventName);
                
                boolean activityState = false;
                
                if (eventName.endsWith("START")){
                    activityState = true;
                }else if (eventName.endsWith("END")){
                    activityState = false;
                }
                
                String activity = evt.getNewValue().toString();
                
                toggleActivity(activityState, activity);
                
            }
        }
        else if (source instanceof PetModel){
        
            if (eventName.equals("petDeath")){
                String deathReason = evt.getNewValue().toString();
                worldModel.stopWorld();
                
                int daysSurvived = worldModel.getDays();
                
                worldModel.destroyWorld(petModel.getPetNumber());
                alertPetDeath(deathReason, daysSurvived);
                
            }
            
            if (eventName.equals("hungerAttrChange")){
                Float hungerProcessRatio = (Float)evt.getNewValue();
                hungerProgress.setProgress(hungerProcessRatio);
            }else if (eventName.equals("thirstAttrChange")){
                Float thirstProgressRatio = (Float)evt.getNewValue();
                thirstProgress.setProgress(thirstProgressRatio);
            }else if (eventName.equals("playAttrChange")){
                Float playProgressRatio = (Float)evt.getNewValue();
                playProgress.setProgress(playProgressRatio);
            }
            
            if (eventName.endsWith("CostChange")){
                String tokenCostString = evt.getNewValue().toString();
                Label costLabel = null;
                
                if (eventName.startsWith("hunger")){
                    costLabel = feedPetCostLabel;
                }
                else if (eventName.startsWith("thirst")){
                    costLabel = waterPetCostLabel;
                }
                else if (eventName.startsWith("play")){
                    costLabel = playPetCostLabel;
                }
                
                if (costLabel != null){
                    costLabel.setText(tokenCostString);
                }
            }
            
            if (eventName.equals("petGenderChanged")){
                String gender = evt.getNewValue().toString();
                petGenderText.setText(gender);
            }else if (eventName.equals("petNameChanged")){
                String name = evt.getNewValue().toString();
                petNameText.setText(name);
            }
        }
        
        if (eventName.equals("ERROR")){
            String errorString = evt.getNewValue().toString();
            showError(errorString);
        }
    }
    
    private void loadInWorld(){
        if (this.petModel != null){
            int petNum = this.petModel.getPetNumber();
            worldModel.removePropertyChangeListener(this);
            worldModel = null;
            worldModel = WorldModel.loadWorld(petNum);
        }else{
            worldModel = new WorldModel();
        }
        
        worldModel.addPropertyChangeListener(this);
        worldModel.refreshWorldInformation();
        worldModel.startWorld();
    }
    
    @Override
    public void setPetModel(PetModel petModel) {
        if (this.petModel != null){
            this.petModel.removePropertyChangeListener(this);
            this.petModel.removePropertyChangeListener(petUserInterface);
            this.petModel = null;
        }
        
        this.petModel = petModel;
        this.petModel.addPropertyChangeListener(this);
        this.petModel.addPropertyChangeListener(petUserInterface);
        this.petModel.refreshPetInformation();
        
        loadInWorld();
        
    }

    private void saveWorldAndPet(){
        if (petModel == null){
            return;
        }
            
        if (!petModel.getPetDead()){
            petModel.savePetModel();

            if (worldModel != null && !worldModel.isWorldDestroyed()){
                worldModel.saveWorld(petModel.getPetNumber());
            }
        }
    }
    
    @FXML
    private void goBackToMainMenu(ActionEvent event) {
        saveWorldAndPet();
        
        if (petModel != null){
            petModel.removePropertyChangeListener(this);
            petModel.removePropertyChangeListener(petUserInterface);
            petModel = null;
        }
        
        if (worldModel != null){
            
            if (!worldModel.isWorldDestroyed()){
                worldModel.stopWorld();
            }
            worldModel.removePropertyChangeListener(this);
            
            worldModel = null;
        }
        
        Switchable.remove("MainGameView");
        Switchable.switchTo("MainMenuView");
    }

    
    @FXML
    private void onQuestionAsk(ActionEvent event) {
        
        worldModel.stopWorld();
        try {
            //https://riptutorial.com/javafx/example/28033/creating-custom-dialog
            
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AskQuestionView.fxml"));
            Parent parent = fxmlLoader.load();
            AskQuestionController dialogController = fxmlLoader.getController();
            
            Scene scene = new Scene(parent, 600, 400);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setTitle("Animal Question!");
            
            dialogController.setStage(stage);
            
            stage.showAndWait();
            
            Boolean questionCorrect = dialogController.getQuestionModel().getQuestionCorrect();
            
            worldModel.questionAnswered(questionCorrect);
            
        } catch (IOException ex) {
            Logger.getLogger(MainGameController.class.getName()).log(Level.SEVERE, null, ex);
        }
        worldModel.startWorld();
    }

    @FXML
    private void onPetPlay(ActionEvent event) {
        String actionName = "play";
        performPetAction(actionName);
    }

    @FXML
    private void onFeedPet(ActionEvent event) {
        String actionName = "hunger";
        performPetAction(actionName);
    }

    @FXML
    private void onGivePetWater(ActionEvent event) {
        String actionName = "thirst";
        performPetAction(actionName);
    }

    private void performPetAction(String actionName){
        int tokensToPay = petModel.getTokenCost(actionName);
        int actionAmount = 10;
        boolean tokensPaid = worldModel.payTokens(tokensToPay);
        
        
        if (tokensPaid){
            
            if (actionName.equals("thirst")){
                petModel.waterPet(actionAmount);
            }else if (actionName.equals("hunger")){
                petModel.feedPet(actionAmount);
            }else if(actionName.equals("play")){
                petModel.playWithPet(actionAmount);
            }

            worldModel.startWorldActivity(actionName);
        }
        
    }
    
    @FXML
    private void onUnderstandError(ActionEvent event) {
        clearError();
    }

    private void alertPetDeath( String deathReason, int daysLived) {
        
        Alert petAlert = new Alert(AlertType.ERROR);
        petAlert.setHeaderText("Pet has died.");
        petAlert.setContentText(deathReason + "\n" + "Your pet lived " + Integer.toString(daysLived) +" days.");
        petAlert.setTitle("Pet Death");
        petAlert.setOnCloseRequest((value)->{
            goBackToMainMenu(null);
        });
        
        petAlert.show();
        
    }

    private void toggleActivity(boolean activityState, String activity) {
        
        String petActivity = null;
        
        if (activity.equals("hunger")){
            petActivity = "pet-food.png";
        }else if (activity.equals("thirst")){
            petActivity = "pet-water.png";
        }else if (activity.equals("play")){
            petActivity = "pet-play.png";
        }
        
        if (activityState == false){
            activityImageView.setVisible(false);
            petUserInterface.switchMode("Vertical");
        }else{
            
            if (petActivity != null){
                Image activityImage = new Image (MainGameController.class.getResourceAsStream("/mankbffinalprojectf20/petassets/" + petActivity));
                activityImageView.setImage(activityImage);
                activityImageView.setVisible(true);
                petUserInterface.switchMode("Horizontal");
            }
            
        }
        
        if (petModel != null){
            petModel.refreshPetInformation();
        }
        
    }
    
}
