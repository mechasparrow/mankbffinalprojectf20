/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mankbffinalprojectf20;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author mechasparrow
 */
public class PetModel extends AbstractModel  {
    
    private int petNumber;
    
    private HashMap<PetBodyPart, String> petBodyPartImageUrls = new HashMap<>();
    
    private String petName;
    private Gender petGender;
    
    private boolean petDead = false;
    
    private static final HashMap<PetBodyPart, String> PET_SUFFIXES = new HashMap<PetBodyPart,String>(){{
        put(PetBodyPart.HEAD, "pet-head");
        put(PetBodyPart.BODY, "pet-body");
        put(PetBodyPart.LEGS, "pet-legs");
        put(PetBodyPart.TAIL, "pet-tail");
    }};
    
    private static final float MAX_AMNT = 100.0f;
    
    private static final int HUNGER_COST = 15;
    private static final int THIRST_COST = 20;
    private static final int PLAY_COST = 25;
    
    /* DIMINISH EACH at different rates */
    private static final int DIMINISH_RATE = 5;
    
    private int hunger = 100;
    private int thirst = 100;
    private int play = 100;
    
    PetModel(int petNumber){
        super();
        this.petNumber = petNumber;
        this.petName = "";
        
        initializeBodyParts();
    }
    
    PetModel(){
        this(0);
    }
    
    PetModel(int petNumber, String petName){
        this(petNumber);
        this.petName = petName;
    }
    
    private void initializeBodyParts(){
        for (PetBodyPart bodyPart : PetBodyPart.values()){
            petBodyPartImageUrls.put(bodyPart, "generic" + "-" + PET_SUFFIXES.get(bodyPart) + ".png");
        }
    }
    
    public void logPetInformation(){
        System.out.println("Pet Name: " + petName);
        System.out.println("Pet Gender: " + petGender.toString());
        
        System.out.println("");
        
        System.out.println("Pet Parts: ");
        for (PetBodyPart bodyPart : PetBodyPart.values()){
            System.out.println("Pet " + bodyPart.toString() + ": " + petBodyPartImageUrls.get(bodyPart));
        }
    
    }
    
    //TODO update unselected body parts
    public void updatePet(Gender petGender, String petName){
       
        if (!(petName.isEmpty() || petName.equals("") || petName == null)){
            setPetName(petName);
            setPetGender(petGender);
            super.firePropertyChange("Success", null, petName + " was successfully created!");

            savePetModel();
            
        }else{
            System.out.println("Invalid pet");
            
            
            super.firePropertyChange("Error", null, "Invalid Pet: please enter a name.");
        }
        
    }
    
    public void updatePetBodyPart(PetBodyPart petBodyPart, String bodyPartName){
        updatePetBodyPart(petBodyPart, bodyPartName, true);
    }
    
    public void updatePetBodyPart(PetBodyPart petBodyPart, String bodyPartName, boolean withModification){
        String petBodyPartSuffix = "";
        
        if (petBodyPart != null && bodyPartName != null){
            String previousBodyImageUrl = petBodyPartImageUrls.get(petBodyPart);
            String newBodyPartImageUrl = null;
            
            if (withModification){
                petBodyPartSuffix = "-" + PET_SUFFIXES.get(petBodyPart) + ".png";
            }
            
            newBodyPartImageUrl = bodyPartName + petBodyPartSuffix;
            petBodyPartImageUrls.put(petBodyPart, newBodyPartImageUrl);
                
            super.firePropertyChange(petBodyPart.toString() + "-body-part-changed", previousBodyImageUrl, newBodyPartImageUrl); 
        }
        
    }
    
    public void refreshPetInformation(){
        System.out.println("firing changes");
        
        super.firePropertyChange("PetNumber", 0, petNumber);
        super.firePropertyChange("PetBodyLoaded", null, null);
        
        for (PetBodyPart bodyPart : PetBodyPart.values()){
            super.firePropertyChange(bodyPart.toString() + "-body-part-changed", null, petBodyPartImageUrls.get(bodyPart));
        }

        super.firePropertyChange("petNameChanged", null, petName);
        super.firePropertyChange("petGenderChanged", null, petGender);
        
        super.firePropertyChange("hungerAttrChange", null, hunger/MAX_AMNT);
        super.firePropertyChange("thirstAttrChange", null, thirst/MAX_AMNT);
        super.firePropertyChange("playAttrChange", null, play/MAX_AMNT);
        
        super.firePropertyChange("hungerCostChange", null, costToString(HUNGER_COST));
        super.firePropertyChange("thirstCostChange", null, costToString(THIRST_COST));
        super.firePropertyChange("playCostChange", null, costToString(PLAY_COST));
    }
    
    private static String costToString(int tokenCost){
        return String.format("- %d Tokens", tokenCost);
    }
   
    public int getTokenCost(String actionName){
        if (actionName.equals("hunger")){
            return HUNGER_COST;
        }
        else if (actionName.equals("thirst")){
            return THIRST_COST;
        }
        else if (actionName.equals("play")){
            return PLAY_COST;
        }else{
            return 0;
        }
    }
    
    public void worldTick(){
        
        if (petDead){
            return;
        }
        
        hunger -= DIMINISH_RATE;
        play -= DIMINISH_RATE;
        thirst -= DIMINISH_RATE;
        
        if (hunger <= 0){
            hunger = 0;
            killPet("hunger");
        }
        
        if (play <= 0){
            play = 0;
            killPet("play");
        }
        
        if (thirst <= 0){
            thirst = 0;
            killPet("thirst");
        }
        
        
        super.firePropertyChange("hungerAttrChange", null, hunger/MAX_AMNT);
        super.firePropertyChange("thirstAttrChange", null, thirst/MAX_AMNT);
        super.firePropertyChange("playAttrChange", null, play/MAX_AMNT);
    }
    
    public void feedPet(int amount){
        super.firePropertyChange("petIsBeingFed", null, true);
        
        hunger += amount;
        
        if (hunger >= MAX_AMNT){
            hunger = (int)MAX_AMNT;
        }
        
        super.firePropertyChange("hungerAttrChange", null, hunger/MAX_AMNT);
    }
    
    public void playWithPet(int amount){
        super.firePropertyChange("petIsBeingPlayed", null, true);
        
        play += amount;
        
        if (play >= MAX_AMNT){
            play = (int)MAX_AMNT;
        }
        
        super.firePropertyChange("playAttrChange", null, play/MAX_AMNT);
    }
    
    public void waterPet(int amount){
        super.firePropertyChange("petIsBeingWatered", null, true);
        
        thirst += amount;
        
        if (thirst >= MAX_AMNT){
            thirst = (int)MAX_AMNT;
        }
        
        super.firePropertyChange("thirstAttrChange", null, thirst/MAX_AMNT);
    }
    
    public void setPetNumber(int petNumber){
        int oldPetNumber = this.petNumber;
        this.petNumber = petNumber;
        
        super.firePropertyChange("PetNumber", oldPetNumber, petNumber);
    }
    
    public int getPetNumber(){
        return petNumber;
    }
    
    public void setPetName(String petName){
        String oldPetName = this.petName;
        this.petName = petName;
        
        super.firePropertyChange("petNameChanged", oldPetName, this.petName);
    }
    
    public String getPetName(String petName){
        return petName;
    }
    
    public void setPetGender(Gender petGender){
        Gender oldPetGender = this.petGender;
        this.petGender = petGender;
        
        super.firePropertyChange("petGenderChanged", oldPetGender, this.petGender);
    }
    
    public Gender getPetGender(){
        return petGender;
    }

    public static PetModel loadPetModel(int petNumber){
        PetModel parsedPetModel = null;
        
        String petFileName = "petSave" + Integer.toString(petNumber) + ".json";
        String fileFolderPath = System.getProperty("user.dir") + "/Pets/";
        File petFile = new File(fileFolderPath + petFileName);
            
        boolean fileExists = petFile.exists();

        if (fileExists){
            
            FileInputStream fileInputStream = null;
            
            try {
                fileInputStream = new FileInputStream(petFile);
                
                String fileData = IOUtils.toString(fileInputStream, StandardCharsets.UTF_8.name());
            
                JsonReader jsonReader = Json.createReader(new StringReader(fileData));
                JsonObject jsonPetObject = jsonReader.readObject();
                    
                if (jsonPetObject.containsKey("petNumber")){
                    
                    int jsonPetNumber = jsonPetObject.getJsonNumber("petNumber").intValue();
                    parsedPetModel = new PetModel(jsonPetNumber);
                    
                    if (jsonPetObject.containsKey("petBodyParts")){
                        JsonObject jsonPetBodyPartsObject = jsonPetObject.getJsonObject("petBodyParts");
                        
                        for (PetBodyPart bodyPart : PetBodyPart.values()){
                            if (jsonPetBodyPartsObject.containsKey(bodyPart.toString())){
                                String petImageUrl = jsonPetBodyPartsObject.getString(bodyPart.toString());
                                parsedPetModel.updatePetBodyPart(bodyPart, petImageUrl, false);
                            }
                        }   
                    }
                    
                    if (jsonPetObject.containsKey("petAttributes")){
                        JsonObject jsonPetAttributes = jsonPetObject.getJsonObject("petAttributes");
                        
                        if (jsonPetAttributes.containsKey("hunger")){
                            Integer hunger = jsonPetAttributes.getInt("hunger");
                            parsedPetModel.setHunger(hunger);
                        }
                        if (jsonPetAttributes.containsKey("thirst")){
                            Integer thirst = jsonPetAttributes.getInt("thirst");
                            parsedPetModel.setThirst(thirst);
                        }
                        if (jsonPetAttributes.containsKey("play")){
                            Integer play = jsonPetAttributes.getInt("play");
                            parsedPetModel.setPlay(play);
                        }
                    }
                    
                    if (jsonPetObject.containsKey("petName") && jsonPetObject.containsKey("petGender")){
                        Gender petGender = Gender.valueOf(jsonPetObject.getString("petGender"));
                        String petName = jsonPetObject.getString("petName");
                        
                        parsedPetModel.updatePet(petGender, petName);
                    }
                    
                }else{
                    System.out.println("Unable to parse Pet File");
                }
            }catch (JsonParsingException ex){
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try{
                    fileInputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            
            
        }else{
            System.out.println("Unable to load pet " + petNumber);
        }
        
        System.out.println("Retrieving Pet " + petFileName);

        return parsedPetModel;
    }
    
    public boolean getPetDead(){
        return petDead;
    }
    
    private void killPet(String deathReason){
        if (petDead){
            return;
        }
        
        String petFileName = "petSave" + Integer.toString(petNumber) + ".json";
        String fileFolderPath = System.getProperty("user.dir") + "/Pets/";
        File petFile = new File(fileFolderPath + petFileName);
            
        boolean fileExists = petFile.exists();
        boolean petDeleted = false;
        
        if (fileExists){
            petDeleted = petFile.delete();
        }
        
        petDead = true;
        super.firePropertyChange("petDeath", null, petName + " has died of " + deathReason);
    }
    
    public void savePetModel() {
    
        String serializedPetData = null;
        
        HashMap<String, Object> petMap = new HashMap<>();
        
        HashMap<String, Object> petBodyPartsMap = new HashMap<>();
        
        HashMap<String, Integer> petAttributes = new HashMap<>();
        petAttributes.put("hunger", hunger);
        petAttributes.put("play", play);
        petAttributes.put("thirst", thirst);
        
        for (PetBodyPart bodyPart : PetBodyPart.values()){
            petBodyPartsMap.put(bodyPart.toString(), petBodyPartImageUrls.get(bodyPart));
        }
        
        petMap.put("petNumber", petNumber);
        petMap.put("petName", petName);
        petMap.put("petGender", petGender.toString());
        petMap.put("petBodyParts", petBodyPartsMap);
        petMap.put("petAttributes", petAttributes);
        
        JsonObject petJsonModel = Json.createObjectBuilder(petMap).build();
        serializedPetData = petJsonModel.toString();
        
        FileWriter fileWriter = null;
        String petFileName = "petSave" + Integer.toString(petNumber) + ".json";
        String fileFolderPath = System.getProperty("user.dir") + "/Pets/";
        File fileFolder = new File(fileFolderPath);

        try {
            
            boolean folderExists = fileFolder.exists();
            
            if (!folderExists){
                boolean folderCreated = fileFolder.mkdir();
                if (folderCreated){
                    System.out.println("Pet folder successfully created");
                }else{
                    System.out.println("Err: Unable to create Pet Folder");
                }
            }   
            
            File petFile = new File(fileFolderPath + petFileName);
            
            boolean fileExists = petFile.exists();
            
            if (!fileExists){
                boolean fileCreated = petFile.createNewFile();
                if (fileCreated){
                    System.out.println("Pet " + petFile + " file created!");
                }else{
                    System.out.println("Unable to create " + petFile + " file.");
                }
            }   
            
            fileWriter = new FileWriter(petFile);            
            fileWriter.write(serializedPetData);
            System.out.println("Saving Pet to " + petFileName);
        
        } catch (IOException ex) {
            Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void setHunger(Integer hunger) {
        this.hunger = hunger;
        super.firePropertyChange("hungerAttrChange", null, hunger/MAX_AMNT);
    }

    private void setThirst(Integer thirst) {
        this.thirst = thirst;
        super.firePropertyChange("thirstAttrChange", null, thirst/MAX_AMNT);
    }

    private void setPlay(Integer play) {
        this.play = play;
        super.firePropertyChange("playAttrChange", null, play/MAX_AMNT);
    }
    
}
